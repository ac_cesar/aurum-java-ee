/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatTipoAsentamientoDao;
import org.calfonso.aurum.domain.CatTipoAsentamiento;

/**
 *
 * @author cesar
 */
public class CatTipoAsentamientoDaoImp extends GenericDaoImp<CatTipoAsentamiento> implements CatTipoAsentamientoDao {
   @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    } 

    @Override
    public CatTipoAsentamiento findByName(String nombreTipoAsentamiento) {
        return em.createNamedQuery(CatTipoAsentamiento.FIND_BY_NAME, CatTipoAsentamiento.class)
                .setParameter("nombreTipoAsentamiento", nombreTipoAsentamiento)
                .getSingleResult();
    }
}
