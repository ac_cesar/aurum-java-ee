/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatTipoEstablecimientoDao;
import org.calfonso.aurum.domain.CatTipoEstablecimiento;

/**
 *
 * @author cesar
 */
public class CatTipoEstablecimientoDaoImp extends GenericDaoImp<CatTipoEstablecimiento> implements CatTipoEstablecimientoDao {
    @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public CatTipoEstablecimiento findByName(String name) {
        return em.createNamedQuery(CatTipoEstablecimiento.FIND_BY_NAME, CatTipoEstablecimiento.class)
                .setParameter("catTipoEstablecimiento", name)
                .getSingleResult();
    }
}
