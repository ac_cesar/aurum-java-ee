/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatEntidadFederativaDao;
import org.calfonso.aurum.domain.CatEntidadFederativa;

/**
 *
 * @author cesar
 */

public class CatEntidadFederativaDaoImp extends GenericDaoImp<CatEntidadFederativa> implements CatEntidadFederativaDao {
    @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
