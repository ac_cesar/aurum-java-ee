/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatPlazaDao;
import org.calfonso.aurum.domain.CatPlaza;

/**
 *
 * @author cesar
 */

public class CatPlazaDaoImp extends GenericDaoImp<CatPlaza> implements CatPlazaDao {
    @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public CatPlaza findByName(String name) {
        return em.createNamedQuery(CatPlaza.FIND_BY_NAME, CatPlaza.class)
                .setParameter("nombrePlaza", name)
                .getSingleResult();
    }
}
