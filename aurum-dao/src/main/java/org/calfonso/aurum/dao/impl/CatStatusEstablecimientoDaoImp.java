/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatStatusEstablecimientoDao;
import org.calfonso.aurum.domain.CatStatusEstablecimiento;

/**
 *
 * @author cesar
 */

public class CatStatusEstablecimientoDaoImp extends GenericDaoImp<CatStatusEstablecimiento> implements CatStatusEstablecimientoDao {
    @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
