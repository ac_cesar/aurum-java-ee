/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatDireccionDao;
import org.calfonso.aurum.domain.CatDireccion;
import org.calfonso.aurum.domain.CatTipoAsentamiento;

/**
 *
 * @author cesar
 */
public class CatDireccionDaoImp extends GenericDaoImp<CatDireccion> implements CatDireccionDao {
    @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<CatDireccion> findByNombreDireccionAndTipoAsentamiento(String nombreDireccion, CatTipoAsentamiento catTipoAsentamiento) {
        return em.createNamedQuery(CatDireccion.FIND_BY_NOMBRE_DIRECCION_AND_NOMBRE_TIPO_ASENTAMIENTO, CatDireccion.class)
                .setParameter("idTipoAsentamiento", catTipoAsentamiento)
                .setParameter("nombreAsentamiento", "%" + nombreDireccion + "%")
                .getResultList();
    }

    @Override
    public List<CatDireccion> findByCodigoPostal(String codigoPostal) {
        return em.createNamedQuery(CatDireccion.FIND_BY_CODIGO_POSTAL, CatDireccion.class)
                .setParameter("codigoPostal", codigoPostal)
                .getResultList();
    }
}
