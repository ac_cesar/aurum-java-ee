/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao;

import java.util.List;
import org.calfonso.aurum.domain.CatDireccion;
import org.calfonso.aurum.domain.CatTipoAsentamiento;

/**
 *
 * @author cesar
 */
public interface CatDireccionDao extends GenericDao<CatDireccion>{
    List<CatDireccion> findByNombreDireccionAndTipoAsentamiento(String nombreDireccion, CatTipoAsentamiento catTipoAsentamiento);
    public List<CatDireccion> findByCodigoPostal(String codigoPostal);
}
