/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao;

import org.calfonso.aurum.domain.CatPlaza;

/**
 *
 * @author cesar
 */
public interface CatPlazaDao extends GenericDao<CatPlaza>{
    CatPlaza findByName(String name);
}
