/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.calfonso.aurum.dao.CatMunicipioDao;
import org.calfonso.aurum.domain.CatMunicipio;
import org.calfonso.aurum.domain.CatPlaza;

/**
 *
 * @author cesar
 */

public class CatMunicipioDaoImp extends GenericDaoImp<CatMunicipio> implements CatMunicipioDao {
    @PersistenceContext(unitName = "dev-testPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<CatMunicipio> findByPlaza(CatPlaza catPlaza) {
        return em.createNamedQuery(CatMunicipio.FIND_BY_PLAZA, CatMunicipio.class)
                .setParameter("catPlaza", catPlaza)
                .getResultList();
    }

    @Override
    public CatMunicipio findByName(String name) {
        return em.createNamedQuery(CatMunicipio.FIND_BY_NAME, CatMunicipio.class)
                .setParameter("nombreMunicipio", name)
                .getSingleResult();
    }
}
