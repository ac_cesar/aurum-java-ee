/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service;

import java.util.List;
import org.calfonso.aurum.domain.CatDireccion;

/**
 *
 * @author cesar
 */
public interface CatDireccionService extends GenericService<CatDireccion>{
    List<CatDireccion> findByNombreDireccionAndNombreTipoAsentamiento(String nombreDireccion, String nombreTipoAsentamiento);
    List<CatDireccion> findByCodigoPostal(String codigoPostal);
}
