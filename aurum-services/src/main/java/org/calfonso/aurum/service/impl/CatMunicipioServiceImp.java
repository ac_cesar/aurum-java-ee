/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatMunicipioDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatMunicipio;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.service.CatMunicipioService;

/**
 *
 * @author cesar
 */
@Local(CatMunicipioService.class)
@Stateless
public class CatMunicipioServiceImp extends GenericServiceImp<CatMunicipio> implements CatMunicipioService, Serializable {
    @Inject
    private CatMunicipioDao catMunicipioDao;
    
    @Override
    protected GenericDao<CatMunicipio> getGenericDao() {
        return catMunicipioDao;
    }

    @Override
    public List<CatMunicipio> findByPlaza(CatPlaza catPlaza) {
        return catMunicipioDao.findByPlaza(catPlaza);
    }

    @Override
    public CatMunicipio findByName(String name) {
        return catMunicipioDao.findByName(name);
    }

    
}
