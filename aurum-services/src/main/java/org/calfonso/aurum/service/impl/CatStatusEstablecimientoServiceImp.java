/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;


import java.io.Serializable;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatStatusEstablecimientoDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatStatusEstablecimiento;
import org.calfonso.aurum.service.CatStatusEstablecimientoService;

/**
 *
 * @author cesar
 */

public class CatStatusEstablecimientoServiceImp extends GenericServiceImp<CatStatusEstablecimiento> implements CatStatusEstablecimientoService, Serializable {
    @Inject
    private CatStatusEstablecimientoDao catStatusEstablecimientoDao;
    
    @Override
    protected GenericDao<CatStatusEstablecimiento> getGenericDao() {
        return catStatusEstablecimientoDao;
    }
    
}
