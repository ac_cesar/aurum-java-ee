/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatDireccionDao;
import org.calfonso.aurum.dao.CatTipoAsentamientoDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatDireccion;
import org.calfonso.aurum.service.CatDireccionService;
import org.calfonso.aurum.service.CatTipoAsentamientoService;

/**
 *
 * @author cesar
 */
@Local(CatDireccionService.class)
@Stateless
public class CatDireccionServiceImp extends GenericServiceImp<CatDireccion> implements Serializable, CatDireccionService{
    @Inject private CatDireccionDao catDireccionDao;
    @Inject private CatTipoAsentamientoDao catTipoAsentamientoDao;
    
    @Override
    protected GenericDao<CatDireccion> getGenericDao() {
        return catDireccionDao;
    }

    @Override
    public List<CatDireccion> findByNombreDireccionAndNombreTipoAsentamiento(String nombreDireccion, String nombreTipoAsentamiento) {
        return catDireccionDao.findByNombreDireccionAndTipoAsentamiento(
                nombreDireccion, 
                catTipoAsentamientoDao.findByName(nombreTipoAsentamiento));
    }

    @Override
    public List<CatDireccion> findByCodigoPostal(String codigoPostal) {
        return catDireccionDao.findByCodigoPostal(codigoPostal);
    }
    
}
