/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service;

import java.util.List;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.domain.CatTipoEstablecimiento;

/**
 *
 * @author cesar
 */
public interface AppSharedState {
    List<CatPlaza> getTodasPlazas();
    List<CatTipoEstablecimiento> getTodosTiposEstablecimiento();
}
