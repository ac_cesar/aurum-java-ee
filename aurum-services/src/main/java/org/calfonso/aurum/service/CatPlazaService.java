/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service;

import org.calfonso.aurum.domain.CatPlaza;

/**
 *
 * @author cesar
 */
public interface CatPlazaService extends GenericService<CatPlaza>{
    CatPlaza findByName(String name);
}
