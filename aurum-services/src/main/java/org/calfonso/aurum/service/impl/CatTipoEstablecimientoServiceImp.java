/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;


import java.io.Serializable;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatTipoEstablecimientoDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatTipoEstablecimiento;
import org.calfonso.aurum.service.CatTipoEstablecimientoService;

/**
 *
 * @author cesar
 */
@Local(CatTipoEstablecimientoService.class)
@Stateless
public class CatTipoEstablecimientoServiceImp extends GenericServiceImp<CatTipoEstablecimiento> implements CatTipoEstablecimientoService, Serializable {
    @Inject 
    private CatTipoEstablecimientoDao catTipoEstablecimientoDao;
    
    @Override
    protected GenericDao<CatTipoEstablecimiento> getGenericDao() {
        return catTipoEstablecimientoDao;
    }

    @Override
    public CatTipoEstablecimiento findByName(String name) {
        return catTipoEstablecimientoDao.findByName(name);
    }
}
