/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.io.Serializable;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatTipoAsentamientoDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatTipoAsentamiento;
import org.calfonso.aurum.service.CatTipoAsentamientoService;

/**
 *
 * @author cesar
 */
@Local(CatTipoAsentamientoService.class)
@Stateless
public class CatTipoAsentamientoServiceImp extends GenericServiceImp<CatTipoAsentamiento> implements CatTipoAsentamientoService, Serializable{
    @Inject
    private CatTipoAsentamientoDao catTipoAsentamientoDao;
    
    @Override
    protected GenericDao<CatTipoAsentamiento> getGenericDao() {
        return catTipoAsentamientoDao;
    }

    @Override
    public CatTipoAsentamiento findByName(String nombreTipoAsentamiento) {
        return catTipoAsentamientoDao.findByName(nombreTipoAsentamiento);
    }
    
}
