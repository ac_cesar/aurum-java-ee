/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service;

import java.util.List;

/**
 *
 * @author cesar
 */
public interface GenericService<T> {
    void create(T entity);
    void update(T entity);
    void delete(T entity);
    T find(Object id);
    List<T> findAll();
    int count();
}
