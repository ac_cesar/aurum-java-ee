/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service;


import java.util.List;
import org.calfonso.aurum.domain.CatMunicipio;
import org.calfonso.aurum.domain.CatPlaza;

/**
 *
 * @author cesar
 */

public interface CatMunicipioService extends GenericService<CatMunicipio>{
    List<CatMunicipio> findByPlaza(CatPlaza catPlaza);
    CatMunicipio findByName(String name);
}
