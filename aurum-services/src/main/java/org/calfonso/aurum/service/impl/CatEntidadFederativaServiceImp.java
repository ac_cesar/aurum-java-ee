/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.io.Serializable;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatEntidadFederativaDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatEntidadFederativa;
import org.calfonso.aurum.service.CatEntidadFederativaService;

/**
 *
 * @author cesar
 */

public class CatEntidadFederativaServiceImp extends GenericServiceImp<CatEntidadFederativa> implements CatEntidadFederativaService, Serializable {
    @Inject
    private CatEntidadFederativaDao catEntidadFederativaDao;
    
    @Override
    protected GenericDao<CatEntidadFederativa> getGenericDao() {
        return catEntidadFederativaDao;
    }
    
}
