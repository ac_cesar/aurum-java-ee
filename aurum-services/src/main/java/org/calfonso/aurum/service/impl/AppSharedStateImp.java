/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.domain.CatTipoEstablecimiento;
import org.calfonso.aurum.service.AppSharedState;
import org.calfonso.aurum.service.CatPlazaService;
import org.calfonso.aurum.service.CatTipoEstablecimientoService;

/**
 *
 * @author cesar
 */
@Named("appCache")
@ApplicationScoped
@Singleton
@Startup
@Local(AppSharedState.class)
public class AppSharedStateImp implements AppSharedState{
    
    private List<CatPlaza> todasPlazas;
    private List<CatTipoEstablecimiento> todosTiposEstablecimiento;
    
    @Inject
    public void inicializar(CatPlazaService plazaService, CatTipoEstablecimientoService tipoEstablecimientoService){
        todasPlazas = plazaService.findAll();
        todosTiposEstablecimiento = tipoEstablecimientoService.findAll();
        System.out.println("SharedStateImp.todasPlazas = " + todasPlazas);
    }
    
    @Override
    public List<CatPlaza> getTodasPlazas() {
        System.out.println("getTodasPlzas()");
        return todasPlazas;
    }

    @Override
    public List<CatTipoEstablecimiento> getTodosTiposEstablecimiento() {
        return todosTiposEstablecimiento;
    }
    
}
