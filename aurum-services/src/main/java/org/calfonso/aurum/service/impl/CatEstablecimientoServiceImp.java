/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.io.Serializable;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatEstablecimientoDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatEstablecimiento;
import org.calfonso.aurum.service.CatEstablecimientoService;

/**
 *
 * @author cesar
 */
public class CatEstablecimientoServiceImp extends GenericServiceImp<CatEstablecimiento> implements CatEstablecimientoService, Serializable {
    @Inject
    private CatEstablecimientoDao catEstablecimientoDao;
    
    @Override
    protected GenericDao<CatEstablecimiento> getGenericDao() {
        return catEstablecimientoDao;
    }
    
}
