/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.io.Serializable;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.calfonso.aurum.dao.CatPlazaDao;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.service.CatPlazaService;

/**
 *
 * @author cesar
 */
@Local(CatPlazaService.class)
@Stateless
public class CatPlazaServiceImp extends GenericServiceImp<CatPlaza> implements CatPlazaService, Serializable {
    @Inject
    private CatPlazaDao catPlazaDao;
    
    @Override
    protected GenericDao<CatPlaza> getGenericDao() {
        return catPlazaDao;
    }

    @Override
    public CatPlaza findByName(String name) {
        return catPlazaDao.findByName(name);
    }
    
}
