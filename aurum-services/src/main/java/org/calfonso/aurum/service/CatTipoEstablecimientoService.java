/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service;

import org.calfonso.aurum.domain.CatTipoEstablecimiento;

/**
 *
 * @author cesar
 */
public interface CatTipoEstablecimientoService extends GenericService<CatTipoEstablecimiento>{
    CatTipoEstablecimiento findByName(String name);
}
