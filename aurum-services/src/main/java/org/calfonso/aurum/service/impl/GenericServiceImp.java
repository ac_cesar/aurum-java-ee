/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.service.impl;

import java.util.List;
import org.calfonso.aurum.dao.GenericDao;
import org.calfonso.aurum.service.GenericService;

/**
 *
 * @author cesar
 */
public abstract class GenericServiceImp<T> implements GenericService<T>{
    protected abstract GenericDao<T> getGenericDao();    
    
    @Override
    public void create(T entity) {
        getGenericDao().create(entity);
    }

    @Override
    public void update(T entity) {
        getGenericDao().update(entity);
    }

    @Override
    public void delete(T entity) {
        getGenericDao().delete(entity);
    }

    @Override
    public T find(Object id) {
        return getGenericDao().find(id);
    }

    @Override
    public List<T> findAll() {
        return getGenericDao().findAll();
    }

    @Override
    public int count() {
        return getGenericDao().count();
    }
    
}
