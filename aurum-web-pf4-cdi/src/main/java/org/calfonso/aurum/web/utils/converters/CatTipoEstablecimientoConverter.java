/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.utils.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.calfonso.aurum.domain.CatTipoEstablecimiento;
import org.calfonso.aurum.service.CatTipoEstablecimientoService;

/**
 *
 * @author cesar
 */

@FacesConverter(forClass = CatTipoEstablecimiento.class)
public class CatTipoEstablecimientoConverter implements Converter{
    @Inject private CatTipoEstablecimientoService catTipoEstablecimientoService;

    @Override
    public CatTipoEstablecimiento getAsObject(FacesContext context, UIComponent component, String value) {
        return catTipoEstablecimientoService.findByName(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String nameInstance = null;
        
        if(value instanceof CatTipoEstablecimiento){
            nameInstance = ((CatTipoEstablecimiento)value).toString();
        }
        return nameInstance;
    }
}
