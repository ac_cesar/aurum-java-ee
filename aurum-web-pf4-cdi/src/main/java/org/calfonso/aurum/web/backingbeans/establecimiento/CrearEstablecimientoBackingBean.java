/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.backingbeans.establecimiento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.calfonso.aurum.domain.CatContactoEstablecimiento;
import org.calfonso.aurum.domain.CatDireccion;
import org.calfonso.aurum.domain.CatEstablecimiento;
import org.calfonso.aurum.domain.CatMunicipio;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.domain.CatTelefonoContacto;
import org.calfonso.aurum.service.CatDireccionService;
import org.calfonso.aurum.service.CatMunicipioService;
import org.calfonso.aurum.service.CatPlazaService;
import org.calfonso.aurum.web.qualifiers.ActualModelo;
import org.calfonso.aurum.web.qualifiers.Constante_UI;
import org.calfonso.aurum.web.qualifiers.ColeccionDireccionesVacia;
import org.calfonso.aurum.web.qualifiers.Crear;
import org.calfonso.aurum.web.qualifiers.Establecimiento;
import org.calfonso.aurum.web.utils.FacesUtils;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.menu.MenuItem;


/**
 *
 * @author cesar
 */
@Named
@ViewScoped
public class CrearEstablecimientoBackingBean implements Serializable{
    /*=======================================================================================================================
    Variables Miembro (Estado del Formulario)
    =========================================================================================================================
    */
    //Main Data Model
    @Inject @Establecimiento @Crear @ActualModelo
    private CatEstablecimiento nuevoEstablecimiento;
    
    //Sección Ubicación --General -- UI Data Model
    private CatPlaza plazaSeleccionada;
    private List<CatMunicipio> municipiosEnPlaza;
    
    //Seccion Ubicacion -- Busqueda de Ubicacion -- UI Data Model
    private CriterioBusquedaAsentamiento[] criteriosBusquedaAsentamiento;
    private CriterioBusquedaAsentamiento criterioSeleccionadoBusquedaAsentamiento;
    private String parametroBusquedaUbicacion;

    //Sección Ubicación -- Elegir Direccion -- UI Data Model
    private List<CatDireccion> direccionesEncontradas;
    private CatDireccion direccionSeleccionada;
    
    //Sección Contacto -- General --UI Data Model
    private CatContactoEstablecimiento nuevoContactoEstablecimiento;
    private List<CatContactoEstablecimiento> contactosEstablecimiento;
    private List<CatTelefonoContacto> telefonosNuevoContacto;
    private String tituloOpcionContacto;
    //Seccion Contacto -- General -- UI Element Visual State
    private boolean habilitarNuevoContacto;
    private boolean habilitarVerTodosContactosAgregados;
    private boolean habilitarEdicionContactosAgregados;
    private boolean habilitarEliminarContactosAgregados;
    
    //Seccion Contacto -- Nuevo Contacto -- General
    private int tmpIdNuevoContacto;
    private List<CatTelefonoContacto> ningunTelefonoAgregadoPorRemover;
    //Seccion Contacto -- Nuevo Contacto -- Teléfonos Agregados
    private int tmpIdTelefonoContacto;
    private boolean deshabilitarEliminarTelefonos;
    private boolean habilitarEdicionTelefonos;
    private String idComponenteListaTelefonos;
    //Seccion Contacto -- Nuevo Contacto -- Teléfonos Por Remover
    private boolean renderBotonEliminarTelefonos;
    private boolean deshabilitarBotonEliminarTelefonos;
    private List<CatTelefonoContacto> telefonosSeleccionadosPorRemover;
    //Sección Contacto -- Nuevo Contacto -- Nuevo Teléfono
    private CatTelefonoContacto nuevoTelefonoContacto;
    
    //Seccion Contacto -- Ver Todos -- Telefono Contacto
    private List<CatTelefonoContacto> telefonosContactoSeleccionado;
    
    
    //Seccion Contacto -- Editar Contacto
    
    
    //Seccion Contacto -- Eliminar Contacto
    private List<CatContactoEstablecimiento> contactosPorEliminar;
    
    
    
    /*=======================================================================================================================
    Servicios Utilizados
    =========================================================================================================================
    */
    //Sólo es utilizado en la renderizacion de la página
    //@Inject private CatPlazaService plazaService;
    @Inject private CatMunicipioService municipioService;
    @Inject private CatDireccionService direccionService;
    
    
    public CrearEstablecimientoBackingBean() {
        System.out.println("CrearEstablecimientoForm()---------------------------------------------------------------------");
    }
    
    /*=======================================================================================================================
     Inicializar estado del formulario
     =========================================================================================================================
     */
    @PostConstruct
    public void inicializarFormulario() {
        //inicializarSeccionUbicacion(plazaService, municipioService);
        inicializarSeccionContacto();
    }
   
    /*=======================================================================================================================
    Inicializar Secciones Ajax
    =========================================================================================================================
    */
    //Secccion Contacto -- General
    private void inicializarSeccionContacto(){
        contactosEstablecimiento = new ArrayList<>();
        //nuevoEstablecimiento.setCatContactoEstablecimientoList(contactosEstablecimiento);
        tituloOpcionContacto = OpcionSeccionContacto.NUEVO.getNombreOpcionContacto();
        ningunTelefonoAgregadoPorRemover = new ArrayList<>();
        idComponenteListaTelefonos = ":establecimientoMainForm:contenedorTablasTelefonosAgregadosContacto";
        
        inicializarNuevoContacto();
        //inicializarVerTodosContacto();
        //inicializarEditarContacto();
        //inicializarEliminarContacto();
    }
    
    //Seccion Contacto -- Eliminar Contacto
    private void inicializarEliminarContacto(){
        contactosPorEliminar = new ArrayList<>();
        
        //Inicializar Eliminar Contacto -- Elementos UI
        habilitarNuevoContacto = false;
        habilitarVerTodosContactosAgregados = false;
        habilitarEdicionContactosAgregados = false;
        habilitarEliminarContactosAgregados = true;
    }
    //Seccion Contacto -- Editar Contacto
    private void inicializarEditarContacto(){
        //Inicializar Ver Todos -- Elementos UI
        habilitarNuevoContacto = false;
        habilitarVerTodosContactosAgregados = false;
        habilitarEdicionContactosAgregados = true;
        habilitarEliminarContactosAgregados = false;
        
        idComponenteListaTelefonos = ":establecimientoMainForm:contactosAgregados";
    }
    
    //Seccion Contacto -- Ver Todos
    private void inicializarVerTodosContacto(){
        //Inicializar Ver Todos -- Elementos UI
        habilitarNuevoContacto = false;
        habilitarVerTodosContactosAgregados = true;
        habilitarEdicionContactosAgregados = false;
        habilitarEliminarContactosAgregados = false;
    }
    
    //Seccion Contacto -- Nuevo Contacto
    private void inicializarNuevoContacto(){
        //Inicializar Nuevo Contacto -- General
        nuevoContactoEstablecimiento = new CatContactoEstablecimiento();
        //nuevoContactoEstablecimiento.setIdEstablecimiento(nuevoEstablecimiento);
        telefonosNuevoContacto = new ArrayList<>();
        nuevoContactoEstablecimiento.setCatTelefonoContactoList(telefonosNuevoContacto);
        
        //Inicializar Nuevo Contacto -- General -- Elementos UI
        habilitarNuevoContacto = true;
        habilitarVerTodosContactosAgregados = false;
        habilitarEdicionContactosAgregados = false;
        habilitarEliminarContactosAgregados = false;
        
        inicializarNuevoTelefono();
    }
    
    //Sección Contacto -- Nuevo Contacto -- Inicializar Remover Teléfonos
    public void inicializarRemoverTelefonos(){
        //Preparar tabla para Eliminar Teléfonos
        deshabilitarEliminarTelefonos = false;
        habilitarEdicionTelefonos = false;
        deshabilitarBotonEliminarTelefonos = true;
        FacesUtils.showInfoMessage("Instrucciones", "Por definir");
    }
    
    //Sección Contacto -- Nuevo Contacto -- Inicializar Edición Teléfonos
    public void inicializarEdicionTelefonos(){
        //Preparar tabla para Editar Teléfonos
        deshabilitarEliminarTelefonos = true;
        habilitarEdicionTelefonos = true;
        deshabilitarBotonEliminarTelefonos = true;
        telefonosSeleccionadosPorRemover = ningunTelefonoAgregadoPorRemover;
        FacesUtils.showInfoMessage("Instrucciones", "Elija la celda en la tabla del télefono a editar");
    }
    
    //Sección Contacto -- Nuevo Contacto -- Inicializar Nuevo Telefono Contacto
    public void inicializarNuevoTelefonoNuevoContacto(ActionEvent event){
        inicializarNuevoTelefono();
        nuevoTelefonoContacto.setIdContactoEstablecimiento(nuevoContactoEstablecimiento);
        idComponenteListaTelefonos = ":establecimientoMainForm:contenedorTablasTelefonosAgregadosContacto";
        //contactoEstablecimientoActual = nuevoContactoEstablecimiento;
        //idRequestComponentNuevoTelefono = event.getComponent().getParent().getId();
        //System.out.println("idRequestComponentNuevoTelefono: " + idRequestComponentNuevoTelefono);
    }
    
    public void inicializarNuevoTelefonoContactoAgregado(CatContactoEstablecimiento contactoEstablecimiento){
        inicializarNuevoTelefono();
        System.out.println("contactoEstablecimiento: " + contactoEstablecimiento.getNombreContacto());
        nuevoTelefonoContacto.setIdContactoEstablecimiento(contactoEstablecimiento);
        idComponenteListaTelefonos = ":establecimientoMainForm:contactosAgregados";
        //nuevoTelefonoContacto.setIdContactoEstablecimiento(nuevoContactoEstablecimiento);
        //contactoEstablecimientoActual = null;
        //idRequestComponentNuevoTelefono = event.getComponent().getParent().getId();
        //System.out.println("idRequestComponentNuevoTelefono: " + idRequestComponentNuevoTelefono);
    }
    
    public void inicializarNuevoTelefono(){
        //Inicializar Nuevo Contacto -- Nuevo Teléfono
        nuevoTelefonoContacto = new CatTelefonoContacto();
        nuevoTelefonoContacto.setIdTelefonoContacto(tmpIdTelefonoContacto++);
        
        //Inicializar Nuevo Contacto -- Nuevo Teléfono -- Elementos UI
        deshabilitarEliminarTelefonos = true;
        habilitarEdicionTelefonos = false;
        deshabilitarBotonEliminarTelefonos = true;
        telefonosSeleccionadosPorRemover = ningunTelefonoAgregadoPorRemover;
        
    }
    
    //Seccion Ubicacion -- Inicializar Busqueda de Ubicacion
    @Inject
    public void inicializarSeccionUbicacion(CatPlazaService plazaService, CatMunicipioService municipioService, 
                                            CatDireccionService direccionService, 
                                            @ColeccionDireccionesVacia List<CatDireccion> listaVaciaDirecciones,
                                            @Constante_UI(nombre = "crearEstablecimiento.nombrePlazaInicial") String nombrePlazaInicial,
                                            @Constante_UI(nombre = "crearEstablecimiento.criteriosBusqueda") CriterioBusquedaAsentamiento[] localCriteriosBusquedaAsentamiento)
    {
        plazaSeleccionada = plazaService.findByName(nombrePlazaInicial);
        municipiosEnPlaza = municipioService.findByPlaza(plazaSeleccionada);
        criteriosBusquedaAsentamiento = localCriteriosBusquedaAsentamiento;
        criterioSeleccionadoBusquedaAsentamiento = localCriteriosBusquedaAsentamiento[0];
        direccionesEncontradas = listaVaciaDirecciones;
        
        this.municipioService = municipioService;
        this.direccionService = direccionService;
    }
    
    /*=======================================================================================================================
    Listeners Ajax and Command Components
    =========================================================================================================================
    */
    //Seccion Contacto -- Eliminar Contacto
    public void eliminarContactoAgregado(){
        System.out.println("eliminarContactoAgregado()");
        System.out.println(contactosPorEliminar);
        contactosEstablecimiento.removeAll(contactosPorEliminar);
        contactosPorEliminar.clear();
    }
    
    //Seccion Contacto -- Editar Contacto
    public void onEdicionPropiedadContactoAgregado(ValueChangeEvent event){
        FacesUtils.showMessageOnEdition(event, "Contacto");
    }
    
    //Seccion Contacto -- Agregar/Crear Contacto
    public void agregarNuevoContacto(){
        nuevoContactoEstablecimiento.setIdContactoEstablecimiento(tmpIdNuevoContacto++);
        contactosEstablecimiento.add(nuevoContactoEstablecimiento);
        inicializarNuevoContacto();
        FacesUtils.addMessage("Contacto Agrgegado");
    }
    
    //Seccion Contacto -- Nuevo Contacto -- Editar Telefono
     public void onEdicionTelefonoAgregado(CellEditEvent event){
         FacesUtils.showMessageOnCellEdition(event, "Teléfono");
     }
    
    //Seccion Contacto -- Eliminar Telefonos Seleccionados
    public void onTelefonoUnselected(UnselectEvent event){
        System.out.println("telefonosSeleccionadosPorRemover.size() = " + telefonosSeleccionadosPorRemover.size());
        deshabilitarBotonEliminarTelefonos = telefonosSeleccionadosPorRemover.isEmpty();
    }

    public void onTelefonoSelected(SelectEvent event) {
        System.out.println("telefonosSeleccionadosPorRemover.size(): " + telefonosSeleccionadosPorRemover.size());
        //telefonosSeleccionadosPorRemover.add(telefonoContacto);
        deshabilitarBotonEliminarTelefonos = telefonosSeleccionadosPorRemover.isEmpty();
    }
    
    public void wrapperRemoverTelefonosSeleccionadosContacto(){
        removerTelefonosSeleccionadosContacto(nuevoContactoEstablecimiento);
    }
    
    public void removerTelefonosSeleccionadosContacto(CatContactoEstablecimiento contactoEstablecimiento){
        System.out.println("telefonosSeleccionadosPorRemover.size(): " + telefonosSeleccionadosPorRemover.size());
        System.out.println("contactoEstablecimiento: " + contactoEstablecimiento.getNombreContacto());
        //telefonosNuevoContacto.removeAll(telefonosSeleccionadosPorRemover);
        contactoEstablecimiento.getCatTelefonoContactoList().removeAll(telefonosSeleccionadosPorRemover);
        telefonosSeleccionadosPorRemover.clear();
        deshabilitarBotonEliminarTelefonos = true;
        FacesUtils.addMessage("Teléfono(s) Eliminados");
    }
    
    //Sección Contacto -- Dialog Nuevo Teléfono
    public void agregarTelefonoContacto(){
        nuevoTelefonoContacto.getIdContactoEstablecimiento().getCatTelefonoContactoList().add(nuevoTelefonoContacto);
        //contactoEstablecimientoActual.getCatTelefonoContactoList().add(nuevoTelefonoContacto);
        FacesUtils.addUserActionMessage("Teléfono Almacenado", nuevoTelefonoContacto.getTelefono());
    }
    
    //Seccion Contacto -- Asignar titulo opcion
    public void onOpcionContactoSeleccionada(ActionEvent event){
        tituloOpcionContacto = ((MenuItem)event.getSource()).getValue().toString();
        
        switch(OpcionSeccionContacto.getByName(tituloOpcionContacto)){
            case NUEVO:
                inicializarNuevoContacto();
                break;
            case VER_TODOS:
                inicializarVerTodosContacto();
                break;
            case ELIMINAR:
                inicializarEliminarContacto();
                break;
            case EDITAR:
                inicializarEditarContacto();
                break;       
        }
    }
    
    //Seccion Ubicacion -- Elegir Direccion
    public void salvarDireccionElejida(){
        FacesUtils.addUserActionMessage("Éxito", "Dirección Elejida");
    }
    
    //Sección Ubicación -- Buscar Direccion 
    public void buscarAsentamiento(AjaxBehaviorEvent event){
        switch(criterioSeleccionadoBusquedaAsentamiento){
            case COLONIA:
                direccionesEncontradas = direccionService.findByNombreDireccionAndNombreTipoAsentamiento(parametroBusquedaUbicacion,CriterioBusquedaAsentamiento.COLONIA.toString());
                break;
            case CODIGO_POSTAL:
                direccionesEncontradas = direccionService.findByCodigoPostal(parametroBusquedaUbicacion);
                break;
        }
    }
    
    //Sección Ubicación
    public void onChangeSelectedPlaza(AjaxBehaviorEvent event){
        municipiosEnPlaza = municipioService.findByPlaza(plazaSeleccionada);    
    }
    
    /*=======================================================================================================================
    Getters and Setters
    =========================================================================================================================
     */
    //Seccion Contacto -- Eliminar Contacto
    
    public boolean isHabilitarEliminarContactosAgregados() {
        return habilitarEliminarContactosAgregados;
    }

    public List<CatContactoEstablecimiento> getContactosPorEliminar() {
        return contactosPorEliminar;
    }

    public void setContactosPorEliminar(List<CatContactoEstablecimiento> contactosPorEliminar) {
        this.contactosPorEliminar = contactosPorEliminar;
    }
    
    
    
    //Seccion Contacto -- Editar Contacto
    
    public String getIdComponenteListaTelefonos() {    
        return idComponenteListaTelefonos;
    }

    public boolean isHabilitarEdicionContactosAgregados() {
        return habilitarEdicionContactosAgregados;
    }

    //Seccion Contacto -- Ver Todos
    public boolean isHabilitarVerTodosContactosAgregados() {    
        return habilitarVerTodosContactosAgregados;
    }

    public List<CatTelefonoContacto> getTelefonosContactoSeleccionado() {
        return telefonosContactoSeleccionado;
    }

    
    //Seccion Contacto -- Nuevo Contacto -- Telefonos Eliminados
    public boolean isRenderBotonEliminarTelefonos() {
        return renderBotonEliminarTelefonos;
    }

    public boolean isDeshabilitarBotonEliminarTelefonos() {
        return deshabilitarBotonEliminarTelefonos;
    }

    public List<CatTelefonoContacto> getTelefonosSeleccionadosPorRemover() {
        return telefonosSeleccionadosPorRemover;
    }

    public void setTelefonosSeleccionadosPorRemover(List<CatTelefonoContacto> telefonosSeleccionadosPorRemover) {
        this.telefonosSeleccionadosPorRemover = telefonosSeleccionadosPorRemover;
    }

    
    //Seccion Contacto -- Nuevo Contacto -- Telefonos Agregados
    public boolean isHabilitarEdicionTelefonos() {
        return habilitarEdicionTelefonos;
    }

    public boolean isDeshabilitarEliminarTelefonos() {
        return deshabilitarEliminarTelefonos;
    }
    
    
    //Seccion Contacto -- General
    public List<CatContactoEstablecimiento> getContactosEstablecimiento() {    
        return contactosEstablecimiento;
    }

    public boolean isHabilitarNuevoContacto() {
        return habilitarNuevoContacto;
    }
    
    public String getOpcionEliminarContacto(){
        return OpcionSeccionContacto.ELIMINAR.getNombreOpcionContacto();
    }
    
    public String getOpcionEditarContacto(){
        return OpcionSeccionContacto.EDITAR.getNombreOpcionContacto();
    }

    public String getOpcionContactoVerTodos() {
        return OpcionSeccionContacto.VER_TODOS.getNombreOpcionContacto();
    }
    
    public String getOpcionContactoNuevo() {
        return OpcionSeccionContacto.NUEVO.getNombreOpcionContacto();
    }

    public String getTituloOpcionContacto() {
        return tituloOpcionContacto;
    }
    

    //Sección Contacto -- Nuevo Contacto 
    public List<CatTelefonoContacto> getTelefonosNuevoContacto() {
        return telefonosNuevoContacto;
    }

    public CatContactoEstablecimiento getNuevoContactoEstablecimiento() {
        return nuevoContactoEstablecimiento;
    }

    
    //Seccción Contacto -- Nuevo Contacto --  Nuevo Telefono
    public CatTelefonoContacto getNuevoTelefonoContacto() {
        return nuevoTelefonoContacto;
    }
    
    
    //Sección Ubicación -- Dialog Elección Dirección
    public CatDireccion getDireccionSeleccionada() {
        return direccionSeleccionada;
    }

    public void setDireccionSeleccionada(CatDireccion direccionSeleccionada) {
        this.direccionSeleccionada = direccionSeleccionada;
    }
    
    public List<CatDireccion> getDireccionesEncontradas() {
        return direccionesEncontradas;
    }
    
    
    //Sección Ubicación
    public String getParametroBusquedaUbicacion() {
        return parametroBusquedaUbicacion;
    }

    public void setParametroBusquedaUbicacion(String parametroBusquedaUbicacion) {
        this.parametroBusquedaUbicacion = parametroBusquedaUbicacion;
    }

    public CriterioBusquedaAsentamiento getCriterioSeleccionadoBusquedaAsentamiento() {
        return criterioSeleccionadoBusquedaAsentamiento;
    }

    public void setCriterioSeleccionadoBusquedaAsentamiento(CriterioBusquedaAsentamiento opcionSeleccionadaBusquedaEstabelcimiento) {
        this.criterioSeleccionadoBusquedaAsentamiento = opcionSeleccionadaBusquedaEstabelcimiento;
    }

    public CriterioBusquedaAsentamiento[] getCriteriosBusquedaAsentamiento() {
        return criteriosBusquedaAsentamiento;
    }

    public List<CatMunicipio> getMunicipiosEnPlaza() {
        return municipiosEnPlaza;
    }

    public CatPlaza getPlazaSeleccionada() {
        return plazaSeleccionada;
    }

    public void setPlazaSeleccionada(CatPlaza plazaSeleccionada) {
        this.plazaSeleccionada = plazaSeleccionada;
    }
    
    
    //Sección Establecimiento
    public CatEstablecimiento getNuevoEstablecimiento() {
        return nuevoEstablecimiento;
    }

    
    /*=======================================================================================================================
    Inner Classes
    =========================================================================================================================
     */
    
        
    private enum OpcionSeccionContacto {

        NUEVO("Nuevo"), VER_TODOS("Ver Todos"), ELIMINAR("Eliminar"), EDITAR("Editar");
        private final String nombreOpcionContacto;

        private OpcionSeccionContacto(String nombreOpcionContacto) {
            this.nombreOpcionContacto = nombreOpcionContacto;
        }

        public String getNombreOpcionContacto() {
            return nombreOpcionContacto;
        }
        
        public static OpcionSeccionContacto getByName(String enumName){
            for(OpcionSeccionContacto opcionSeccionContacto : OpcionSeccionContacto.values()){
                if(opcionSeccionContacto.getNombreOpcionContacto().equals(enumName)){
                    return opcionSeccionContacto;
                }
            }
            return null;
        }
    }
    
    /*========================================================================================================================
    Disposicion de Recursos
    ==========================================================================================================================
    */
    @PreDestroy
    public void dispose(){
        System.out.println(" backing bean muerto");
    }
    /*========================================================================================================================
    Inyección de instancias como CDI Beans
    ==========================================================================================================================
    */
   
}
