/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.backingbeans.establecimiento;

/**
 *
 * @author cesar
 */
public enum CriterioBusquedaAsentamiento {

    COLONIA("Colonia"), CODIGO_POSTAL("Codigo Postal");
    private final String nombreCriterioAsentamiento;

    private CriterioBusquedaAsentamiento(String nombreColonia) {
        this.nombreCriterioAsentamiento = nombreColonia;
    }

    public String getNombreCriterioAsentamiento() {
        return nombreCriterioAsentamiento;
    }

    public static CriterioBusquedaAsentamiento getByName(String enumName) {
        for (CriterioBusquedaAsentamiento criterio : CriterioBusquedaAsentamiento.values()) {
            if (criterio.getNombreCriterioAsentamiento().equals(enumName)) {
                return criterio;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return nombreCriterioAsentamiento;
    }
}
