/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.backingbeans.establecimiento;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import org.calfonso.aurum.domain.CatDireccion;
import org.calfonso.aurum.domain.CatEstablecimiento;
import org.calfonso.aurum.web.qualifiers.VistaModelo;
import org.calfonso.aurum.web.qualifiers.ActualModelo;
import org.calfonso.aurum.web.qualifiers.ColeccionDireccionesVacia;
import org.calfonso.aurum.web.qualifiers.Constante_UI;
import org.calfonso.aurum.web.qualifiers.Crear;
import org.calfonso.aurum.web.qualifiers.Establecimiento;
import org.calfonso.aurum.web.qualifiers.ModeloEntidad;



/**
 *
 * @author cesar
 */
public class CrearEstablecimientoBackingProducer {
    @Produces
    @Constante_UI(nombre = "crearEstablecimiento.criteriosBusqueda")
    private static final CriterioBusquedaAsentamiento[] CRITERIOS_BUSQUEDA = CriterioBusquedaAsentamiento.values();
    
    @Produces 
    @Constante_UI(nombre = "crearEstablecimiento.nombrePlazaInicial")
    private static final String NOMBRE_PLAZA_DEFUALT = "Mexico";
    
    
    @Produces @Establecimiento @Crear @ActualModelo
    public CatEstablecimiento nuevoEstablecimiento(){
        CatEstablecimiento nuevoEstablecimiento = new CatEstablecimiento();
        return nuevoEstablecimiento;
    }
    
    @Produces @ColeccionDireccionesVacia
    public List<CatDireccion>  listaVaciaDirecciones(){
        return new ArrayList<>();
    }
}
