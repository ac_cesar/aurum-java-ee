/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.utils;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.service.CatTipoEstablecimientoService;

/**
 *
 * @author cesar
 */
@ApplicationScoped
@Named
public class DataSharedUi {
    private List<CatPlaza> todasPlazas;
    
    public DataSharedUi(){
        System.out.println("DataSharedUi()-----------------------");
    }

    public List<CatPlaza> getTodasPlazas() {
        if(todasPlazas == null){
            //BeansUtils.getBeanServiceByClassName(CatTipoEstablecimientoService.class.getSimpleName() + "Imp", CatTipoEstablecimientoService.class.getCanonicalName());
            todasPlazas = new ArrayList<>();
        } 
            
        return todasPlazas;
    }
    
    
}
