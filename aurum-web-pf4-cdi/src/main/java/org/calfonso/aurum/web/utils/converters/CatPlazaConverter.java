/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.utils.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import org.calfonso.aurum.domain.CatPlaza;
import org.calfonso.aurum.service.CatPlazaService;

/**
 *
 * @author cesar
 */
@FacesConverter(forClass = CatPlaza.class)
public class CatPlazaConverter implements Converter{
    @Inject CatPlazaService catPlazaService;

    @Override
    public CatPlaza getAsObject(FacesContext context, UIComponent component, String value) {
        CatPlaza catPlaza = catPlazaService.findByName(value);
        return catPlaza;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof CatPlaza ? value.toString() : null;
    }
    
}
