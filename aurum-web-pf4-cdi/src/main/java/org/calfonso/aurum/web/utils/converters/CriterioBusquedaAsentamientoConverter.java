/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.utils.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.calfonso.aurum.web.backingbeans.establecimiento.CriterioBusquedaAsentamiento;

/**
 *
 * @author cesar
 */
@FacesConverter(forClass = CriterioBusquedaAsentamiento.class)
public class CriterioBusquedaAsentamientoConverter implements Converter{

    @Override
    public CriterioBusquedaAsentamiento getAsObject(FacesContext context, UIComponent component, String value) {
        return CriterioBusquedaAsentamiento.getByName(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof CriterioBusquedaAsentamiento ? value.toString() : null;
    }
    
}
