/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.controllers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.calfonso.aurum.domain.CatEstablecimiento;
import org.calfonso.aurum.service.CatEstablecimientoService;
import org.calfonso.aurum.web.qualifiers.ActualModelo;
import org.calfonso.aurum.web.qualifiers.ValorInicialModelo;

/**
 *
 * @author cesar
 */
@Named
@RequestScoped
public class EstablecimientoController {
    @Inject @ActualModelo private CatEstablecimiento establecimiento;
    @Inject private CatEstablecimientoService establecimientoService;
    
    public String guardarEstablecimiento(){
        establecimientoService.create(establecimiento);
        return null;
    }
}
