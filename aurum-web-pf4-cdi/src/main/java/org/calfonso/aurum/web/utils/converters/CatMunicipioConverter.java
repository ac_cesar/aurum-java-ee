/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.utils.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import org.calfonso.aurum.domain.CatMunicipio;
import org.calfonso.aurum.service.CatMunicipioService;

/**
 *
 * @author cesar
 */
@FacesConverter(forClass = CatMunicipio.class)
public class CatMunicipioConverter implements Converter{
    @Inject CatMunicipioService catMunicipioService;

    @Override
    public CatMunicipio getAsObject(FacesContext context, UIComponent component, String value) {
        return catMunicipioService.findByName(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof CatMunicipio ? value.toString() : null;
    }
    
}
