/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.web.utils;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author cesar
 */
public class FacesUtils {
    
    public static SelectItem[] getSelectItems(List<?> options){
        SelectItem[] optionsSelectItem = new SelectItem[options.size() + 1];
        optionsSelectItem[0] = new SelectItem(null, "-- --");
        
        for(int index = 1; index < options.size(); index++){
            optionsSelectItem[index] = new SelectItem(options.get(index));
        }
        return optionsSelectItem;
    }
    
    public static void showInfoMessage(String titulo, String message){
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, message);
        RequestContext.getCurrentInstance().showMessageInDialog(facesMessage);
    }
    
    public static void addMessage(String message){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
    }
    
    public static void addUserActionMessage(String tituloMensaje, String mensaje){
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, tituloMensaje, mensaje);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }
    
    public static void showMessageOnCellEdition(CellEditEvent event, String nombreConcepto){
        Object valorAnterior = event.getOldValue();
        Object nuevoValor = event.getNewValue();
        
        if(nuevoValor != null && !nuevoValor.equals(valorAnterior)){
            addUserActionMessage(nombreConcepto + " Editado", "Anterior: " + valorAnterior + ", Nuevo: " + nuevoValor);
        }
    }
    
    public static void showMessageOnEdition(ValueChangeEvent event, String nombreConcepto){
        Object valorAnterior = event.getOldValue();
        Object nuevoValor = event.getNewValue();
        
        if(nuevoValor != null && !nuevoValor.equals(valorAnterior)){
            addUserActionMessage(nombreConcepto + " Editado", "Anterior: " + valorAnterior + ", Nuevo: " + nuevoValor);
        }
    }
    
}
