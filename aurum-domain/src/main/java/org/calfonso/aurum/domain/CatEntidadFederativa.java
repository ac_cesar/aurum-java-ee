/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catEntidadFederativa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEntidadFederativa.findAll", query = "SELECT c FROM CatEntidadFederativa c"),
    @NamedQuery(name = "CatEntidadFederativa.findByIdEntidadFederativa", query = "SELECT c FROM CatEntidadFederativa c WHERE c.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "CatEntidadFederativa.findByCatEntidadFederativa", query = "SELECT c FROM CatEntidadFederativa c WHERE c.catEntidadFederativa = :catEntidadFederativa")})
public class CatEntidadFederativa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEntidadFederativa")
    private Short idEntidadFederativa;
    @Size(max = 512)
    @Column(name = "catEntidadFederativa")
    private String catEntidadFederativa;
    @OneToMany(mappedBy = "idEntidadFederativa")
    private List<CatMunicipio> catMunicipioList;

    public CatEntidadFederativa() {
    }

    public CatEntidadFederativa(Short idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public Short getIdEntidadFederativa() {
        return idEntidadFederativa;
    }

    public void setIdEntidadFederativa(Short idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public String getCatEntidadFederativa() {
        return catEntidadFederativa;
    }

    public void setCatEntidadFederativa(String catEntidadFederativa) {
        this.catEntidadFederativa = catEntidadFederativa;
    }

    @XmlTransient
    public List<CatMunicipio> getCatMunicipioList() {
        return catMunicipioList;
    }

    public void setCatMunicipioList(List<CatMunicipio> catMunicipioList) {
        this.catMunicipioList = catMunicipioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntidadFederativa != null ? idEntidadFederativa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEntidadFederativa)) {
            return false;
        }
        CatEntidadFederativa other = (CatEntidadFederativa) object;
        if ((this.idEntidadFederativa == null && other.idEntidadFederativa != null) || (this.idEntidadFederativa != null && !this.idEntidadFederativa.equals(other.idEntidadFederativa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatEntidadFederativa[ idEntidadFederativa=" + idEntidadFederativa + " ]";
    }
    
}
