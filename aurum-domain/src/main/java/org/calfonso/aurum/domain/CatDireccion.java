/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catDireccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = CatDireccion.FIND_BY_NOMBRE_ASENTAMIENTO, query = "SELECT c FROM CatDireccion c WHERE c.nombreAsentamiento = :nombreAsentamiento"),
    @NamedQuery(name = CatDireccion.FIND_BY_CODIGO_POSTAL, query = "SELECT c FROM CatDireccion c WHERE c.codigoPostal = :codigoPostal"),
    @NamedQuery(name = CatDireccion.FIND_BY_NOMBRE_DIRECCION_AND_NOMBRE_TIPO_ASENTAMIENTO, query = "SELECT c FROM CatDireccion c WHERE c.idTipoAsentamiento = :idTipoAsentamiento AND c.nombreAsentamiento LIKE :nombreAsentamiento")
})
public class CatDireccion implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String FIND_BY_NOMBRE_ASENTAMIENTO = "CatDireccion.findByNombreAserntamiento";
    public static final String FIND_BY_CODIGO_POSTAL = "CatDireccion.findByCodigoPostal";
    public static final String FIND_BY_NOMBRE_DIRECCION_AND_NOMBRE_TIPO_ASENTAMIENTO = "CatDireccion.findByNombreDireccionAndNombreTipoAsentamiento";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDireccion")
    private Integer idDireccion;
    @Size(max = 4096)
    @Column(name = "nombreAsentamiento")
    private String nombreAsentamiento;
    @Size(max = 5)
    @Column(name = "codigoPostal")
    private String codigoPostal;
    @JoinColumn(name = "idTipoAsentamiento", referencedColumnName = "idTipoAsentamiento")
    @ManyToOne
    private CatTipoAsentamiento idTipoAsentamiento;
    @JoinColumn(name = "idMunicpio", referencedColumnName = "idMunicipio")
    @ManyToOne
    private CatMunicipio idMunicipio;
    @OneToMany(mappedBy = "idDireccion")
    private List<CatEstablecimiento> catEstablecimientoList;

    public CatDireccion() {
    }

    public CatDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getNombreAsentamiento() {
        return nombreAsentamiento;
    }

    public void setNombreAsentamiento(String nombreAsentamiento) {
        this.nombreAsentamiento = nombreAsentamiento;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public CatTipoAsentamiento getIdTipoAsentamiento() {
        return idTipoAsentamiento;
    }

    public void setIdTipoAsentamiento(CatTipoAsentamiento idTipoAsentamiento) {
        this.idTipoAsentamiento = idTipoAsentamiento;
    }

    public CatMunicipio getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(CatMunicipio idMunicpio) {
        this.idMunicipio = idMunicpio;
    }

    @XmlTransient
    public List<CatEstablecimiento> getCatEstablecimientoList() {
        return catEstablecimientoList;
    }

    public void setCatEstablecimientoList(List<CatEstablecimiento> catEstablecimientoList) {
        this.catEstablecimientoList = catEstablecimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDireccion != null ? idDireccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDireccion)) {
            return false;
        }
        CatDireccion other = (CatDireccion) object;
        if ((this.idDireccion == null && other.idDireccion != null) || (this.idDireccion != null && !this.idDireccion.equals(other.idDireccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatDireccion[ idDireccion=" + idDireccion + " ]";
    }
    
}
