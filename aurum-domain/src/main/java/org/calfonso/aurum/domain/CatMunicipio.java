/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catMunicipio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatMunicipio.findAll", query = "SELECT c FROM CatMunicipio c"),
    @NamedQuery(name = "CatMunicipio.findByIdMunicipio", query = "SELECT c FROM CatMunicipio c WHERE c.idMunicipio = :idMunicipio"),
    @NamedQuery(name = CatMunicipio.FIND_BY_NAME, query = "SELECT c FROM CatMunicipio c WHERE c.catMunicipio = :nombreMunicipio"),
    @NamedQuery(name = CatMunicipio.FIND_BY_PLAZA, query = "SELECT c FROM CatMunicipio c WHERE c.idPlaza = :catPlaza")
})
public class CatMunicipio implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String FIND_BY_PLAZA = "CatMunicipio.findByPlaza";
    public static final String FIND_BY_NAME = "CatMuncipio.findByName";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMunicipio")
    private Integer idMunicipio;
    @Size(max = 512)
    @Column(name = "catMunicipio")
    private String catMunicipio;
    @OneToMany(mappedBy = "idMunicipio")
    private List<CatDireccion> catDireccionList;
    @JoinColumn(name = "idPlaza", referencedColumnName = "idPlaza")
    @ManyToOne
    private CatPlaza idPlaza;
    @JoinColumn(name = "idEntidadFederativa", referencedColumnName = "idEntidadFederativa")
    @ManyToOne
    private CatEntidadFederativa idEntidadFederativa;
    @OneToMany(mappedBy = "idMunicipio")
    private List<CatEstablecimiento> catEstablecimientoList;

    public CatMunicipio() {
    }

    public CatMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public Integer getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getCatMunicipio() {
        return catMunicipio;
    }

    public void setCatMunicipio(String catMunicipio) {
        this.catMunicipio = catMunicipio;
    }

    @XmlTransient
    public List<CatDireccion> getCatDireccionList() {
        return catDireccionList;
    }

    public void setCatDireccionList(List<CatDireccion> catDireccionList) {
        this.catDireccionList = catDireccionList;
    }

    public CatPlaza getIdPlaza() {
        return idPlaza;
    }

    public void setIdPlaza(CatPlaza idPlaza) {
        this.idPlaza = idPlaza;
    }

    public CatEntidadFederativa getIdEntidadFederativa() {
        return idEntidadFederativa;
    }

    public void setIdEntidadFederativa(CatEntidadFederativa idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    @XmlTransient
    public List<CatEstablecimiento> getCatEstablecimientoList() {
        return catEstablecimientoList;
    }

    public void setCatEstablecimientoList(List<CatEstablecimiento> catEstablecimientoList) {
        this.catEstablecimientoList = catEstablecimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMunicipio != null ? idMunicipio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatMunicipio)) {
            return false;
        }
        CatMunicipio other = (CatMunicipio) object;
        if ((this.idMunicipio == null && other.idMunicipio != null) || (this.idMunicipio != null && !this.idMunicipio.equals(other.idMunicipio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return catMunicipio;
    }
    
}
