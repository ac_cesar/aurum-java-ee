/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catPlaza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPlaza.findAll", query = "SELECT c FROM CatPlaza c"),
    @NamedQuery(name = "CatPlaza.findByIdPlaza", query = "SELECT c FROM CatPlaza c WHERE c.idPlaza = :idPlaza"),
    @NamedQuery(name = CatPlaza.FIND_BY_NAME, query = "SELECT c FROM CatPlaza c WHERE c.catPlaza = :nombrePlaza")
})
public class CatPlaza implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String FIND_BY_NAME = "CatPlaza.findByName";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPlaza")
    private Integer idPlaza;
    @Size(max = 255)
    @Column(name = "catPlaza")
    private String catPlaza;
    @OneToMany(mappedBy = "idPlaza")
    private List<CatMunicipio> catMunicipioList;

    public CatPlaza() {
    }

    public CatPlaza(Integer idPlaza) {
        this.idPlaza = idPlaza;
    }

    public Integer getIdPlaza() {
        return idPlaza;
    }

    public void setIdPlaza(Integer idPlaza) {
        this.idPlaza = idPlaza;
    }

    public String getCatPlaza() {
        return catPlaza;
    }

    public void setCatPlaza(String catPlaza) {
        this.catPlaza = catPlaza;
    }

    @XmlTransient
    public List<CatMunicipio> getCatMunicipioList() {
        return catMunicipioList;
    }

    public void setCatMunicipioList(List<CatMunicipio> catMunicipioList) {
        this.catMunicipioList = catMunicipioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlaza != null ? idPlaza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPlaza)) {
            return false;
        }
        CatPlaza other = (CatPlaza) object;
        if ((this.idPlaza == null && other.idPlaza != null) || (this.idPlaza != null && !this.idPlaza.equals(other.idPlaza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return catPlaza;
    }
    
}
