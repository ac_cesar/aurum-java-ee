/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catEstablecimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEstablecimiento.findAll", query = "SELECT c FROM CatEstablecimiento c"),
    @NamedQuery(name = "CatEstablecimiento.findByIdEstablecimeinto", query = "SELECT c FROM CatEstablecimiento c WHERE c.idEstablecimeinto = :idEstablecimeinto"),
    @NamedQuery(name = "CatEstablecimiento.findByNombreEstablecimiento", query = "SELECT c FROM CatEstablecimiento c WHERE c.nombreEstablecimiento = :nombreEstablecimiento"),
    @NamedQuery(name = "CatEstablecimiento.findByCalle", query = "SELECT c FROM CatEstablecimiento c WHERE c.calle = :calle"),
    @NamedQuery(name = "CatEstablecimiento.findByNumeroDomicilio", query = "SELECT c FROM CatEstablecimiento c WHERE c.numeroDomicilio = :numeroDomicilio"),
    @NamedQuery(name = "CatEstablecimiento.findByEntreCalle", query = "SELECT c FROM CatEstablecimiento c WHERE c.entreCalle = :entreCalle"),
    @NamedQuery(name = "CatEstablecimiento.findByYCalle", query = "SELECT c FROM CatEstablecimiento c WHERE c.yCalle = :yCalle"),
    @NamedQuery(name = "CatEstablecimiento.findByReferenciaFisica", query = "SELECT c FROM CatEstablecimiento c WHERE c.referenciaFisica = :referenciaFisica")})
public class CatEstablecimiento implements Serializable {
    @OneToMany(mappedBy = "idEstablecimiento")
    private List<CatContactoEstablecimiento> catContactoEstablecimientoList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEstablecimeinto")
    private Integer idEstablecimeinto;
    @Size(max = 2048)
    @Column(name = "nombreEstablecimiento")
    private String nombreEstablecimiento;
    @Size(max = 4096)
    @Column(name = "calle")
    private String calle;
    @Size(max = 128)
    @Column(name = "numeroDomicilio")
    private String numeroDomicilio;
    @Size(max = 4096)
    @Column(name = "entreCalle")
    private String entreCalle;
    @Size(max = 4096)
    @Column(name = "yCalle")
    private String yCalle;
    @Size(max = 4096)
    @Column(name = "referenciaFisica")
    private String referenciaFisica;
    @JoinColumn(name = "idTipoEstablecimiento", referencedColumnName = "idTipoEstablecimiento")
    @ManyToOne
    private CatTipoEstablecimiento idTipoEstablecimiento;
    @JoinColumn(name = "idStatusEstablecimiento", referencedColumnName = "idStatusEstablecimiento")
    @ManyToOne
    private CatStatusEstablecimiento idStatusEstablecimiento;
    @JoinColumn(name = "idMunicipio", referencedColumnName = "idMunicipio")
    @ManyToOne
    private CatMunicipio idMunicipio;
    @JoinColumn(name = "idDireccion", referencedColumnName = "idDireccion")
    @ManyToOne
    private CatDireccion idDireccion;

    public CatEstablecimiento() {
    }

    public CatEstablecimiento(Integer idEstablecimeinto) {
        this.idEstablecimeinto = idEstablecimeinto;
    }

    public Integer getIdEstablecimeinto() {
        return idEstablecimeinto;
    }

    public void setIdEstablecimeinto(Integer idEstablecimeinto) {
        this.idEstablecimeinto = idEstablecimeinto;
    }

    public String getNombreEstablecimiento() {
        return nombreEstablecimiento;
    }

    public void setNombreEstablecimiento(String nombreEstablecimiento) {
        System.out.println("setNombreEstablecimiento()--------------------------------------");    
        this.nombreEstablecimiento = nombreEstablecimiento;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroDomicilio() {
        return numeroDomicilio;
    }

    public void setNumeroDomicilio(String numeroDomicilio) {
        this.numeroDomicilio = numeroDomicilio;
    }

    public String getEntreCalle() {
        return entreCalle;
    }

    public void setEntreCalle(String entreCalle) {
        this.entreCalle = entreCalle;
    }

    public String getYCalle() {
        return yCalle;
    }

    public void setYCalle(String yCalle) {
        this.yCalle = yCalle;
    }

    public String getReferenciaFisica() {
        return referenciaFisica;
    }

    public void setReferenciaFisica(String referenciaFisica) {
        this.referenciaFisica = referenciaFisica;
    }

    public CatTipoEstablecimiento getIdTipoEstablecimiento() {
        return idTipoEstablecimiento;
    }

    public void setIdTipoEstablecimiento(CatTipoEstablecimiento idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }

    public CatStatusEstablecimiento getIdStatusEstablecimiento() {
        return idStatusEstablecimiento;
    }

    public void setIdStatusEstablecimiento(CatStatusEstablecimiento idStatusEstablecimiento) {
        this.idStatusEstablecimiento = idStatusEstablecimiento;
    }

    public CatMunicipio getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(CatMunicipio idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public CatDireccion getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(CatDireccion idDireccion) {
        this.idDireccion = idDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstablecimeinto != null ? idEstablecimeinto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEstablecimiento)) {
            return false;
        }
        CatEstablecimiento other = (CatEstablecimiento) object;
        if ((this.idEstablecimeinto == null && other.idEstablecimeinto != null) || (this.idEstablecimeinto != null && !this.idEstablecimeinto.equals(other.idEstablecimeinto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatEstablecimiento[ idEstablecimeinto=" + idEstablecimeinto + " ]";
    }

    @XmlTransient
    public List<CatContactoEstablecimiento> getCatContactoEstablecimientoList() {
        return catContactoEstablecimientoList;
    }

    public void setCatContactoEstablecimientoList(List<CatContactoEstablecimiento> catContactoEstablecimientoList) {
        this.catContactoEstablecimientoList = catContactoEstablecimientoList;
    }
    
}
