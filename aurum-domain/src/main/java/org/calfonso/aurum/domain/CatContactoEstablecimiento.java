/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catContactoEstablecimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatContactoEstablecimiento.findAll", query = "SELECT c FROM CatContactoEstablecimiento c"),
    @NamedQuery(name = "CatContactoEstablecimiento.findByIdContactoEstablecimiento", query = "SELECT c FROM CatContactoEstablecimiento c WHERE c.idContactoEstablecimiento = :idContactoEstablecimiento"),
    @NamedQuery(name = "CatContactoEstablecimiento.findByNombreContacto", query = "SELECT c FROM CatContactoEstablecimiento c WHERE c.nombreContacto = :nombreContacto"),
    @NamedQuery(name = "CatContactoEstablecimiento.findByApellidoPaterno", query = "SELECT c FROM CatContactoEstablecimiento c WHERE c.apellidoPaterno = :apellidoPaterno"),
    @NamedQuery(name = "CatContactoEstablecimiento.findByApellidoMaterno", query = "SELECT c FROM CatContactoEstablecimiento c WHERE c.apellidoMaterno = :apellidoMaterno"),
    @NamedQuery(name = "CatContactoEstablecimiento.findByRfc", query = "SELECT c FROM CatContactoEstablecimiento c WHERE c.rfc = :rfc"),
    @NamedQuery(name = "CatContactoEstablecimiento.findByEmail", query = "SELECT c FROM CatContactoEstablecimiento c WHERE c.email = :email")})
public class CatContactoEstablecimiento implements Serializable {
    @OneToMany(mappedBy = "idContactoEstablecimiento")
    private List<CatTelefonoContacto> catTelefonoContactoList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idContactoEstablecimiento")
    private Integer idContactoEstablecimiento;
    @Size(max = 512)
    @Column(name = "nombreContacto")
    private String nombreContacto;
    @Size(max = 1024)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Size(max = 1024)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Size(max = 13)
    @Column(name = "RFC")
    private String rfc;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "email")
    private String email;
    @JoinColumn(name = "idEstablecimiento", referencedColumnName = "idEstablecimeinto")
    @ManyToOne
    private CatEstablecimiento idEstablecimiento;
    
    public CatContactoEstablecimiento() {
    }

    public CatContactoEstablecimiento(Integer idContactoEstablecimiento) {
        this.idContactoEstablecimiento = idContactoEstablecimiento;
    }

    public Integer getIdContactoEstablecimiento() {
        return idContactoEstablecimiento;
    }

    public void setIdContactoEstablecimiento(Integer idContactoEstablecimiento) {
        this.idContactoEstablecimiento = idContactoEstablecimiento;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CatEstablecimiento getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(CatEstablecimiento idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }
    
    @Transient
    public String getNombreCompleto(){
        return nombreContacto + " " + apellidoPaterno +  " " + apellidoMaterno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContactoEstablecimiento != null ? idContactoEstablecimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatContactoEstablecimiento)) {
            return false;
        }
        CatContactoEstablecimiento other = (CatContactoEstablecimiento) object;
        if ((this.idContactoEstablecimiento == null && other.idContactoEstablecimiento != null) || (this.idContactoEstablecimiento != null && !this.idContactoEstablecimiento.equals(other.idContactoEstablecimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatContactoEstablecimiento[ idContactoEstablecimiento=" + idContactoEstablecimiento + " ]";
    }

    @XmlTransient
    public List<CatTelefonoContacto> getCatTelefonoContactoList() {
        return catTelefonoContactoList;
    }

    public void setCatTelefonoContactoList(List<CatTelefonoContacto> catTelefonoContactoList) {
        this.catTelefonoContactoList = catTelefonoContactoList;
    }
    
}
