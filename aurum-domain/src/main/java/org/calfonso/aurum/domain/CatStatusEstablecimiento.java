/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catStatusEstablecimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatStatusEstablecimiento.findAll", query = "SELECT c FROM CatStatusEstablecimiento c"),
    @NamedQuery(name = "CatStatusEstablecimiento.findByIdStatusEstablecimiento", query = "SELECT c FROM CatStatusEstablecimiento c WHERE c.idStatusEstablecimiento = :idStatusEstablecimiento"),
    @NamedQuery(name = "CatStatusEstablecimiento.findByCatStatusEstablecimiento", query = "SELECT c FROM CatStatusEstablecimiento c WHERE c.catStatusEstablecimiento = :catStatusEstablecimiento")})
public class CatStatusEstablecimiento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idStatusEstablecimiento")
    private Short idStatusEstablecimiento;
    @Size(max = 512)
    @Column(name = "catStatusEstablecimiento")
    private String catStatusEstablecimiento;
    @OneToMany(mappedBy = "idStatusEstablecimiento")
    private List<CatEstablecimiento> catEstablecimientoList;

    public CatStatusEstablecimiento() {
    }

    public CatStatusEstablecimiento(Short idStatusEstablecimiento) {
        this.idStatusEstablecimiento = idStatusEstablecimiento;
    }

    public Short getIdStatusEstablecimiento() {
        return idStatusEstablecimiento;
    }

    public void setIdStatusEstablecimiento(Short idStatusEstablecimiento) {
        this.idStatusEstablecimiento = idStatusEstablecimiento;
    }

    public String getCatStatusEstablecimiento() {
        return catStatusEstablecimiento;
    }

    public void setCatStatusEstablecimiento(String catStatusEstablecimiento) {
        this.catStatusEstablecimiento = catStatusEstablecimiento;
    }

    @XmlTransient
    public List<CatEstablecimiento> getCatEstablecimientoList() {
        return catEstablecimientoList;
    }

    public void setCatEstablecimientoList(List<CatEstablecimiento> catEstablecimientoList) {
        this.catEstablecimientoList = catEstablecimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatusEstablecimiento != null ? idStatusEstablecimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatStatusEstablecimiento)) {
            return false;
        }
        CatStatusEstablecimiento other = (CatStatusEstablecimiento) object;
        if ((this.idStatusEstablecimiento == null && other.idStatusEstablecimiento != null) || (this.idStatusEstablecimiento != null && !this.idStatusEstablecimiento.equals(other.idStatusEstablecimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatStatusEstablecimiento[ idStatusEstablecimiento=" + idStatusEstablecimiento + " ]";
    }
    
}
