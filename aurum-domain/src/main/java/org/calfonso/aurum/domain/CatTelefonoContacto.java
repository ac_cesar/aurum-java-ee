/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catTelefonoContacto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTelefonoContacto.findAll", query = "SELECT c FROM CatTelefonoContacto c"),
    @NamedQuery(name = "CatTelefonoContacto.findByIdTelefonoContacto", query = "SELECT c FROM CatTelefonoContacto c WHERE c.idTelefonoContacto = :idTelefonoContacto"),
    @NamedQuery(name = "CatTelefonoContacto.findByTelefono", query = "SELECT c FROM CatTelefonoContacto c WHERE c.telefono = :telefono")})
public class CatTelefonoContacto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTelefonoContacto")
    private Integer idTelefonoContacto;
    @Size(max = 30)
    @Column(name = "telefono")
    private String telefono;
    @JoinColumn(name = "idContactoEstablecimiento", referencedColumnName = "idContactoEstablecimiento")
    @ManyToOne
    private CatContactoEstablecimiento idContactoEstablecimiento;

    public CatTelefonoContacto() {
    }

    public CatTelefonoContacto(Integer idTelefonoContacto) {
        this.idTelefonoContacto = idTelefonoContacto;
    }

    public Integer getIdTelefonoContacto() {
        return idTelefonoContacto;
    }

    public void setIdTelefonoContacto(Integer idTelefonoContacto) {
        this.idTelefonoContacto = idTelefonoContacto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public CatContactoEstablecimiento getIdContactoEstablecimiento() {
        return idContactoEstablecimiento;
    }

    public void setIdContactoEstablecimiento(CatContactoEstablecimiento idContactoEstablecimiento) {
        this.idContactoEstablecimiento = idContactoEstablecimiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTelefonoContacto != null ? idTelefonoContacto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTelefonoContacto)) {
            return false;
        }
        CatTelefonoContacto other = (CatTelefonoContacto) object;
        if ((this.idTelefonoContacto == null && other.idTelefonoContacto != null) || (this.idTelefonoContacto != null && !this.idTelefonoContacto.equals(other.idTelefonoContacto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatTelefonoContacto[ idTelefonoContacto=" + idTelefonoContacto + " ]";
    }
    
}
