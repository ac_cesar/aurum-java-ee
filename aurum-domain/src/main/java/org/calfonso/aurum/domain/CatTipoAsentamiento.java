/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catTipoAsentamiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoAsentamiento.findAll", query = "SELECT c FROM CatTipoAsentamiento c"),
    @NamedQuery(name = "CatTipoAsentamiento.findByIdTipoAsentamiento", query = "SELECT c FROM CatTipoAsentamiento c WHERE c.idTipoAsentamiento = :idTipoAsentamiento"),
    @NamedQuery(name = CatTipoAsentamiento.FIND_BY_NAME, query = "SELECT c FROM CatTipoAsentamiento c WHERE c.catTipoAsentamiento = :nombreTipoAsentamiento")})
public class CatTipoAsentamiento implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String FIND_BY_NAME = "CatTipoAsentamiento.findByName";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoAsentamiento")
    private Short idTipoAsentamiento;
    @Size(max = 512)
    @Column(name = "catTipoAsentamiento")
    private String catTipoAsentamiento;
    @OneToMany(mappedBy = "idTipoAsentamiento")
    private List<CatDireccion> catDireccionList;

    public CatTipoAsentamiento() {
    }

    public CatTipoAsentamiento(Short idTipoAsentamiento) {
        this.idTipoAsentamiento = idTipoAsentamiento;
    }

    public Short getIdTipoAsentamiento() {
        return idTipoAsentamiento;
    }

    public void setIdTipoAsentamiento(Short idTipoAsentamiento) {
        this.idTipoAsentamiento = idTipoAsentamiento;
    }

    public String getCatTipoAsentamiento() {
        return catTipoAsentamiento;
    }

    public void setCatTipoAsentamiento(String catTipoAsentamiento) {
        this.catTipoAsentamiento = catTipoAsentamiento;
    }

    @XmlTransient
    public List<CatDireccion> getCatDireccionList() {
        return catDireccionList;
    }

    public void setCatDireccionList(List<CatDireccion> catDireccionList) {
        this.catDireccionList = catDireccionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoAsentamiento != null ? idTipoAsentamiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoAsentamiento)) {
            return false;
        }
        CatTipoAsentamiento other = (CatTipoAsentamiento) object;
        if ((this.idTipoAsentamiento == null && other.idTipoAsentamiento != null) || (this.idTipoAsentamiento != null && !this.idTipoAsentamiento.equals(other.idTipoAsentamiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.calfonso.aurum.domain.CatTipoAsentamiento[ idTipoAsentamiento=" + idTipoAsentamiento + " ]";
    }
    
}
