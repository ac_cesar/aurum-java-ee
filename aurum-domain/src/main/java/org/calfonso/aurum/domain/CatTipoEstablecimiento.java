/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.calfonso.aurum.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cesar
 */
@Entity
@Table(name = "catTipoEstablecimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoEstablecimiento.findAll", query = "SELECT c FROM CatTipoEstablecimiento c"),
    @NamedQuery(name = "CatTipoEstablecimiento.findByIdTipoEstablecimiento", query = "SELECT c FROM CatTipoEstablecimiento c WHERE c.idTipoEstablecimiento = :idTipoEstablecimiento"),
    @NamedQuery(name = CatTipoEstablecimiento.FIND_BY_NAME, query = "SELECT c FROM CatTipoEstablecimiento c WHERE c.catTipoEstablecimiento = :catTipoEstablecimiento")})
public class CatTipoEstablecimiento implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String FIND_BY_NAME = "CatTipoEstablecimiento.findByName";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoEstablecimiento")
    private Short idTipoEstablecimiento;
    @Size(max = 255)
    @Column(name = "catTipoEstablecimiento")
    private String catTipoEstablecimiento;
    @OneToMany(mappedBy = "idTipoEstablecimiento")
    private List<CatEstablecimiento> catEstablecimientoList;

    public CatTipoEstablecimiento() {
    }

    public CatTipoEstablecimiento(Short idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }

    public Short getIdTipoEstablecimiento() {
        return idTipoEstablecimiento;
    }

    public void setIdTipoEstablecimiento(Short idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }

    public String getCatTipoEstablecimiento() {
        return catTipoEstablecimiento;
    }

    public void setCatTipoEstablecimiento(String catTipoEstablecimiento) {
        this.catTipoEstablecimiento = catTipoEstablecimiento;
    }

    @XmlTransient
    public List<CatEstablecimiento> getCatEstablecimientoList() {
        return catEstablecimientoList;
    }

    public void setCatEstablecimientoList(List<CatEstablecimiento> catEstablecimientoList) {
        this.catEstablecimientoList = catEstablecimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoEstablecimiento != null ? idTipoEstablecimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoEstablecimiento)) {
            return false;
        }
        CatTipoEstablecimiento other = (CatTipoEstablecimiento) object;
        if ((this.idTipoEstablecimiento == null && other.idTipoEstablecimiento != null) || (this.idTipoEstablecimiento != null && !this.idTipoEstablecimiento.equals(other.idTipoEstablecimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return catTipoEstablecimiento;
    }
    
}
